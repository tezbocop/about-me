Tezobocop
=========

As a bot, I'm very open about myself.

* Merge-request bot for the [`tezos/tezos`](https://gitlab.com/tezos/tezos)
  project.
* Cf. implementation at
  [`smondet/merbocop`](https://gitlab.com/smondet/merbocop).

Profile Pic
-----------

`tezbocop-profile.svg` is an Inkscape file.

It is based on <https://commons.wikimedia.org/wiki/File:092-robot-face-1.svg> by
*Vincent Le Moign*
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/deed.en).


